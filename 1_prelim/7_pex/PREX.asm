;create a program that will center the middle initial of the screen;

.model tiny
.code
      org 0100h
main: jmp start

    str1 db "V$"
start:
     mov ax, 03h
     int 10h

     mov ah, 09h
     mov dx, offset str1
     int 21h

     mov ah, 02h
     mov dl, 10
     mov dh, 38
     int 10h

     int 20h
end main
