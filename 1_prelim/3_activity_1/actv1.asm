; FILENAME      : ACTV1.ASM
; PROGRAMMED BY : CASTILLO, PAUL MARK D.
; DATE STARTED  : SEPT. 22, 2005
; DATE FINISHED : ----------------------

.model tiny
.code
    org 0100h

main: jmp start
str1 db "Assembly Language$"

start:
     mov ax, 03h
     int 10h

     mov ah, 02h
     mov bh, 0
     mov dh, 12
     mov dl, 30
     int 10h

     mov ah, 09h
     mov dx, offset str1
     int 21h

     int 20h
     end main
end

