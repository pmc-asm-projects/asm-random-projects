; FILENAME      : ACTV2.ASM
; PROGRAMMED BY : CASTILLO, PAUL MARK D.
; DATE STARTED  : SEPT. 22, 2005
; DATE FINISHED : ----------------------

.model tiny
.code
    org 0100h

main: jmp start
str1 db "Assembly Language$"
str2 db "                                    AMA$"
str3 db "                            COMPUTER UNIVERSITY$"
str4 db "                  Villa Arca Street. Project 08, Quezon City$"
str5 db "                                 CSCI06CA$"
str6 db "       Computer Organization Architecture & Assembly Language Programming$"
str7 db "                           Castillo, Paul Mark D.$"
str8 db "                                  Student$"
str9 db "                          Prof. Jerico B. Lorico$"
str10 db "                                Instructor$"
str11 db "                             September 15, 2005$"

start:
     mov ax, 03h
     int 10h

     mov ah, 02h
     mov bh, 0
     mov dh, 1
     mov dl, 1
     int 10h

     mov ah, 09h
     mov dx, offset str1
     int 21h

     mov ah, 02h
     mov bh, 0
     mov dh, 2
     mov dl, 1
     int 10h

     mov ah, 09h
     mov dx, offset str2
     int 21h

     mov ah, 02h
     mov bh, 0
     mov dh, 3
     mov dl, 1
     int 10h

     mov ah, 09h
     mov dx, offset str3
     int 21h


     mov ah, 02h
     mov bh, 0
     mov dh, 4
     mov dl, 1
     int 10h

     mov ah, 09h
     mov dx, offset str4
     int 21h


     mov ah, 02h
     mov bh, 0
     mov dh, 5
     mov dl, 1
     int 10h

     mov ah, 09h
     mov dx, offset str5
     int 21h


     mov ah, 02h
     mov bh, 0
     mov dh, 6
     mov dl, 1
     int 10h

     mov ah, 09h
     mov dx, offset str6
     int 21h


     mov ah, 02h
     mov bh, 0
     mov dh, 7
     mov dl, 1
     int 10h

     mov ah, 09h
     mov dx, offset str7
     int 21h


     mov ah, 02h
     mov bh, 0
     mov dh, 8
     mov dl, 1
     int 10h

     mov ah, 09h
     mov dx, offset str8
     int 21h


     mov ah, 02h
     mov bh, 0
     mov dh, 9
     mov dl, 1
     int 10h

     mov ah, 09h
     mov dx, offset str9
     int 21h


     mov ah, 02h
     mov bh, 0
     mov dh, 10
     mov dl, 1
     int 10h

     mov ah, 09h
     mov dx, offset str10
     int 21h


     mov ah, 02h
     mov bh, 0
     mov dh, 11
     mov dl, 1
     int 10h

     mov ah, 09h
     mov dx, offset str11
     int 21h



     int 20h
     end main
end





